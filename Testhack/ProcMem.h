#ifndef PROCMEM_H //If Not Defined
#define PROCMEM_H //Define Now

#define WIN32_LEAN_AND_MEAN //Excludes Headers We Wont Use (Increase Compile Time)

#include <windows.h> //Standard Windows Functions/Data Types
#include <iostream> //Constains Input/Output Functions (cin/cout etc..)
#include <TlHelp32.h> //Contains Read/Write Functions
#include <string> //Support For Strings
#include <sstream> //Supports Data Conversion

class ProcMem{
protected:

	//STORAGE
	HANDLE hProcess;
	DWORD dwPID, dwProtection, dwCaveAddress;

	//MISC
	BOOL bPOn, bIOn, bProt;

public:

	//MISC FUNCTIONS
	ProcMem();
	~ProcMem();
    int chSizeOfArray(char *chArray); 
	int iSizeOfArray(int *iArray); 
	bool iFind(int *iAry, int iVal); 

#pragma region TEMPLATE MEMORY FUNCTIONS

	//REMOVE READ/WRITE PROTECTION
	template <class cData>
	void Protection(DWORD dwAddress)
	{	   
		if(!bProt)
			VirtualProtectEx(hProcess, (LPVOID)dwAddress, sizeof(cData), PAGE_EXECUTE_READWRITE, &dwProtection); 
		else
			VirtualProtectEx(hProcess, (LPVOID)dwAddress, sizeof(cData), dwProtection, &dwProtection); 

		bProt = !bProt;
	}

	//READ MEMORY 
	template <class cData>
	cData Read(DWORD dwAddress)
	{
		cData cRead; 
		ReadProcessMemory(hProcess, (LPVOID)dwAddress, &cRead, sizeof(cData), NULL); 
		return cRead; 
	}
    	
	//READ MEMORY - Pointer
	template <class cData>
	cData Read(DWORD dwAddress, char *Offset, BOOL Type)
	{
		//Variables
		int iSize = iSizeOfArray(Offset) -1; 
        dwAddress = Read<DWORD>(dwAddress); 

		//Loop Through Each Offset & Store Hex Value (Address)
		for (int i = 0; i < iSize; i++)	
			dwAddress = Read<DWORD>(dwAddress + Offset[i]);

		if (!Type)
			return dwAddress + Offset[iSize]; 
		else
			return Read<cData>(dwAddress + Offset[iSize]); 
	}

	//WRITE MEMORY
	template <class cData>
    void Write(DWORD dwAddress, cData Value)
	{ 	
		WriteProcessMemory(hProcess, (LPVOID)dwAddress, &Value, sizeof(cData), NULL); 
	}

	//WRITE MEMORY - Pointer
	template <class cData>
	void Write(DWORD dwAddress, char *Offset, cData Value)
	{ 
			Write<cData>(Read<cData>(dwAddress, Offset, false), Value); 
	}
	
	//MEMORY FUNCTION PROTOTYPES
	virtual void Process(char* ProcessName); 
	virtual void Patch(DWORD dwAddress, char *chPatch_Bts, char *chDefault_Bts); 
	virtual void Inject(DWORD dwAddress, char *chInj_Bts, char *chDef_Bts, BOOL Type); 
	virtual DWORD AOB_Scan(DWORD dwAddress, DWORD dwEnd, char *chPattern); 
	virtual DWORD Module(LPSTR ModuleName); 

#pragma endregion	

};
#endif

