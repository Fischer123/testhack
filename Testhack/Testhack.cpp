#include "Testhack.h"
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <msclr\marshal.h>
#include "ProcMem.h"
#include "OffsetsAndVariables.h"

ProcMem Mem;

#pragma comment(lib, "user32.lib")

using namespace std;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Threading;
using namespace msclr::interop;

void GetKeyInput();
void ReadData();
void GlowESP();
void NoFlash();
void Bunnyhop();
void Triggerbot();
void Radar();
void TriggerShotWPM();
void TriggerShotME();
void TriggerBurstWPM();
void TriggerBurstME();
GlowStruct GetTeamColor();
GlowStruct GetEnemyColor();


GlowStruct CRed = { 0.4f, 0.f, 0.f, 1.f, true, false };
GlowStruct CGreen = { 0.f, 0.4f, 0.f, 1.f, true, false };
GlowStruct CBlue = { 0.f, 0.f, 0.4f, 1.f, true, false };
GlowStruct CYellow = { 0.4f, 0.4f, 0.f, 1.f, true, false };
GlowStruct COrange = { 1.f, 0.4666f, 0.f, 1.f, true, false };
GlowStruct CPurple = { 0.3647f, 0.f, 1.f, 1.f, true, false };
GlowStruct CWhite = { 1.f, 1.f, 1.f, 1.f, true, false };

[STAThread]
void Main(array<String^>^ args) 
{
	Mem.Process("csgo.exe");

	Client = Mem.Module("client.dll");

	ThreadStart ^ t_ReadData = gcnew ThreadStart(&ReadData);
	Thread ^ ReadDataThread = gcnew Thread(t_ReadData);
	ReadDataThread->Start();

	ThreadStart ^ t_GlowESP = gcnew ThreadStart(&GlowESP);
	Thread ^ GlowESPThread = gcnew Thread(t_GlowESP);
	GlowESPThread->Start();

	ThreadStart ^ t_NoFlash = gcnew ThreadStart(&NoFlash);
	Thread ^ NoFlashThread = gcnew Thread(t_NoFlash);
	NoFlashThread->Start();

	ThreadStart ^ t_Bunnyhop = gcnew ThreadStart(&Bunnyhop);
	Thread ^ BunnyhopThread = gcnew Thread(t_Bunnyhop);
	BunnyhopThread->Start();

	ThreadStart ^ t_Triggerbot = gcnew ThreadStart(&Triggerbot);
	Thread ^ TriggerbotThread = gcnew Thread(t_Triggerbot);
	TriggerbotThread->Start();

	ThreadStart ^ t_Radar = gcnew ThreadStart(&Radar);
	Thread ^ RadarThread = gcnew Thread(t_Radar);
	RadarThread->Start();

	ThreadStart ^ t_KeyInput = gcnew ThreadStart(&GetKeyInput);
	Thread ^ KeyInputThread = gcnew Thread(t_KeyInput);
	KeyInputThread->Start();

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	Testhack::Testhack form;
	Application::Run(%form);

	ReadDataThread->Abort();
	GlowESPThread->Abort();
	NoFlashThread->Abort();
	BunnyhopThread->Abort();
	TriggerbotThread->Abort();
	RadarThread->Abort();
	KeyInputThread->Abort();
}

void Radar()
{
	while (true)
	{
		for (int r = 0; r < 64; r++)
		{

			/* r_currentPlayer */
			DWORD r_currentPlayer = Mem.Read<DWORD>(Client + EntityList + ((r - 1) * 16));
			/* r_currentPlayerDormant */
			bool r_currentPlayerDormant = Mem.Read<bool>(r_currentPlayer + isDormant);
			/* r_currentPlayerbSpotted */
			bool r_currentPlayerbSpotted = Mem.Read<bool>(r_currentPlayer + bSpotted);
			
			if (r_currentPlayerDormant)
			{
				continue;
			}
			else
			{
				if (!r_currentPlayerbSpotted)
				{
					Mem.Write<bool>(r_currentPlayer + bSpotted, 1);
				}
				else
				{
					continue;
				}
			}
		}

		Sleep(1);
	}
}

void GetKeyInput()
{
	while (true)
	{
		if (GetAsyncKeyState(TriggerKey) & 1)
		{
			TriggerToggle = !TriggerToggle;
		}

		Sleep(500);
	}
}

void ReadData()
{
	while (true)
	{
		LocalBase = Mem.Read<DWORD>(Client + LocalPlayer);
		/* GlowPointer */
		GlowPointer = Mem.Read<DWORD>(Client + GlowObjectBase);
		/* LocalPlayer_Team */
		LocalPlayer_Team = Mem.Read<int>(LocalBase + m_iTeamNum);
		/* inCross */
		inCross = Mem.Read<int>(LocalBase + m_iCrossHairID);
		/* Trigger_EntityBase */
		Trigger_EntityBase = Mem.Read<DWORD>(Client + EntityList + ((inCross - 1) * 16));
		/* Trigger_EntityBase_Team */
		Trigger_EntityBase_TeamID = Mem.Read<int>(Trigger_EntityBase + m_iTeamNum);
		/* FlashMaxAlpha */
		FlashMaxAlpha = Mem.Read<float>(LocalBase + flashMaxAlpha);
		/* fFlags */
		fFlag = Mem.Read<byte>(LocalBase + m_fFlags);

		Sleep(1);
	}
}

void DrawGlow(int GlowIndex, GlowStruct Color)
{

	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x4)), Color.r);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x8)), Color.g);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0xC)), Color.b);
	Mem.Write<float>((GlowPointer + ((GlowIndex * 0x34) + 0x10)), Color.a);
	Mem.Write<BOOL>((GlowPointer + ((GlowIndex * 0x34) + 0x24)), Color.rwo);
	Mem.Write<BOOL>((GlowPointer + ((GlowIndex * 0x34) + 0x25)), Color.rwuo);
}

void GlowESP()
{
	while (true)
	{
		if (GlowEnabled)
		{
			for (int g = 0; g < 64; g++)
			{
				/* Read the current player's address in memory */
				DWORD g_currentPlayer = Mem.Read<DWORD>(Client + EntityList + ((g - 1) * 16));

				/* Read if the Entity is dormant */
				bool g_currentPlayer_Dormant = Mem.Read<bool>(g_currentPlayer + isDormant);

				/* Glow Index of the current player */
				int g_currentPlayer_GlowIndex = Mem.Read<int>(g_currentPlayer + GlowIndex);

				/* Read the current player's team */
				int g_currentPlayer_Team = Mem.Read<int>(g_currentPlayer + m_iTeamNum);

				/* Check if the current player is dormant */
				if (g_currentPlayer_Dormant)
				{
					continue;
				}
				else
				{
					/* Team off && Enemy off */
					if (!GlowShowTeam && !GlowShowEnemys)
					{
						continue;
					}
					/* Team on && Enemy's on */
					else if (GlowShowTeam && GlowShowEnemys)
					{
						/* If g_currentPlayer is in the enemy team */
						if (g_currentPlayer_Team != LocalPlayer_Team)
						{					
							/* Pass the GlowIndex of the current player and call GetEnemyColor which returns the set Color */
							DrawGlow(g_currentPlayer_GlowIndex, GetEnemyColor());
						}
						else
						{
							/* Pass the GlowIndex of the current player and call GetTeamColor which returns the set Color */
							DrawGlow(g_currentPlayer_GlowIndex, GetTeamColor());
						}
					}
					else if (GlowShowTeam && !GlowShowEnemys)
					{
						/* If g_currentPlayer is in the enemy team */
						if (g_currentPlayer_Team != LocalPlayer_Team)
						{
							continue;
						}
						/* If g_currentPlayer is in my team */
						else
						{
							DrawGlow(g_currentPlayer_GlowIndex, GetTeamColor());
						}
					}
					/* Team off && Enemy's on */
					else if (!GlowShowTeam && GlowShowEnemys)
					{
						/* If g_currentPlayer is in the enemy team */
						if (g_currentPlayer_Team != LocalPlayer_Team)
						{
							DrawGlow(g_currentPlayer_GlowIndex, GetEnemyColor());
						}
						/* If g_currentPlayer is in my team */
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}
				}

			}
		}

		Sleep(1);
	}
}

GlowStruct GetTeamColor()
{
	GlowStruct TeamColor;

	if (GlowTeamColor == 1)
	{
		TeamColor.r = CRed.r;
		TeamColor.g = CRed.g;
		TeamColor.b = CRed.b;
		TeamColor.a = CRed.a;
		TeamColor.rwo = CRed.rwo;
		TeamColor.rwuo = CRed.rwuo;
	}
	else if (GlowTeamColor == 2)
	{
		TeamColor.r = CGreen.r;
		TeamColor.g = CGreen.g;
		TeamColor.b = CGreen.b;
		TeamColor.a = CGreen.a;
		TeamColor.rwo = CGreen.rwo;
		TeamColor.rwuo = CGreen.rwuo;
	}
	else if (GlowTeamColor == 3)
	{
		TeamColor.r = CBlue.r;
		TeamColor.g = CBlue.g;
		TeamColor.b = CBlue.b;
		TeamColor.a = CBlue.a;
		TeamColor.rwo = CBlue.rwo;
		TeamColor.rwuo = CBlue.rwuo;
	}
	else if (GlowTeamColor == 4)
	{
		TeamColor.r = CYellow.r;
		TeamColor.g = CYellow.g;
		TeamColor.b = CYellow.b;
		TeamColor.a = CYellow.a;
		TeamColor.rwo = CYellow.rwo;
		TeamColor.rwuo = CYellow.rwuo;
	}
	else if (GlowTeamColor == 5)
	{
		TeamColor.r = COrange.r;
		TeamColor.g = COrange.g;
		TeamColor.b = COrange.b;
		TeamColor.a = COrange.a;
		TeamColor.rwo = COrange.rwo;
		TeamColor.rwuo = COrange.rwuo;
	}
	else if (GlowTeamColor == 6)
	{
		TeamColor.r = CPurple.r;
		TeamColor.g = CPurple.g;
		TeamColor.b = CPurple.b;
		TeamColor.a = CPurple.a;
		TeamColor.rwo = CPurple.rwo;
		TeamColor.rwuo = CPurple.rwuo;
	}
	else if (GlowTeamColor == 7)
	{
		TeamColor.r = CWhite.r;
		TeamColor.g = CWhite.g;
		TeamColor.b = CWhite.b;
		TeamColor.a = CWhite.a;
		TeamColor.rwo = CWhite.rwo;
		TeamColor.rwuo = CWhite.rwuo;
	}
	else
	{
		TeamColor.r = CGreen.r;
		TeamColor.g = CGreen.g;
		TeamColor.b = CGreen.b;
		TeamColor.a = CGreen.a;
		TeamColor.rwo = CGreen.rwo;
		TeamColor.rwuo = CGreen.rwuo;
	}

	return TeamColor;
}

GlowStruct GetEnemyColor()
{
	GlowStruct EnemyColor;

	if (GlowEnemyColor == 1)
	{
		EnemyColor.r = CRed.r;
		EnemyColor.g = CRed.g;
		EnemyColor.b = CRed.b;
		EnemyColor.a = CRed.a;
		EnemyColor.rwo = CRed.rwo;
		EnemyColor.rwuo = CRed.rwuo;
	}
	else if (GlowEnemyColor == 2)
	{
		EnemyColor.r = CGreen.r;
		EnemyColor.g = CGreen.g;
		EnemyColor.b = CGreen.b;
		EnemyColor.a = CGreen.a;
		EnemyColor.rwo = CGreen.rwo;
		EnemyColor.rwuo = CGreen.rwuo;
	}
	else if (GlowEnemyColor == 3)
	{
		EnemyColor.r = CBlue.r;
		EnemyColor.g = CBlue.g;
		EnemyColor.b = CBlue.b;
		EnemyColor.a = CBlue.a;
		EnemyColor.rwo = CBlue.rwo;
		EnemyColor.rwuo = CBlue.rwuo;
	}
	else if (GlowEnemyColor == 4)
	{
		EnemyColor.r = CYellow.r;
		EnemyColor.g = CYellow.g;
		EnemyColor.b = CYellow.b;
		EnemyColor.a = CYellow.a;
		EnemyColor.rwo = CYellow.rwo;
		EnemyColor.rwuo = CYellow.rwuo;
	}
	else if (GlowEnemyColor == 5)
	{
		EnemyColor.r = COrange.r;
		EnemyColor.g = COrange.g;
		EnemyColor.b = COrange.b;
		EnemyColor.a = COrange.a;
		EnemyColor.rwo = COrange.rwo;
		EnemyColor.rwuo = COrange.rwuo;
	}
	else if (GlowEnemyColor == 6)
	{
		EnemyColor.r = CPurple.r;
		EnemyColor.g = CPurple.g;
		EnemyColor.b = CPurple.b;
		EnemyColor.a = CPurple.a;
		EnemyColor.rwo = CPurple.rwo;
		EnemyColor.rwuo = CPurple.rwuo;
	}
	else if (GlowEnemyColor == 7)
	{
		EnemyColor.r = CWhite.r;
		EnemyColor.g = CWhite.g;
		EnemyColor.b = CWhite.b;
		EnemyColor.a = CWhite.a;
		EnemyColor.rwo = CWhite.rwo;
		EnemyColor.rwuo = CWhite.rwuo;
	}
	else
	{
		EnemyColor.r = CRed.r;
		EnemyColor.g = CRed.g;
		EnemyColor.b = CRed.b;
		EnemyColor.a = CRed.a;
		EnemyColor.rwo = CRed.rwo;
		EnemyColor.rwuo = CRed.rwuo;
	}

	return EnemyColor;
}

void NoFlash()
{
	while (true)
	{
		if (NoFlashEnabled)
		{
			/* Check if LocalPlayer is flashed */
			if (FlashMaxAlpha > 0.f)
			{
				/* Remove Flash by setting the value back to 0.f */
				Mem.Write<float>(LocalBase + flashMaxAlpha, 0.f);
			}
		}

		Sleep(1);
	}
}

void Bunnyhop()
{
	while (true)
	{
		/* Check if Bunnyhop is enabled */
		if (BunnyEnabled)
		{
			/* Check if BunnyKey is pressed */
			if (GetAsyncKeyState(BunnyKey))
			{
				if (fFlag == 1 || fFlag == 3 || fFlag == 5 || fFlag == 7)
				{
					/* Check if BunnyUseWriteProcessMemory is enabled */
					if (BunnyUseWPM)
					{
						/* +jump */
						Mem.Write<int>(Client + jump, 1);
						Sleep(60);
						/* -jump */
						Mem.Write<int>(Client + jump, 0);
					}
					else
					{
						keybd_event(VK_SPACE, 0x39, NULL, NULL);
						Sleep(60);
						keybd_event(VK_SPACE, 0x39, KEYEVENTF_KEYUP, NULL);
					}
				}
			}
		}

		Sleep(1);
	}
}

void Triggerbot()
{
	while (true)
	{
		if (TriggerEnabled)
		{
			if (TriggerToggleCheckbox)
			{
				if (TriggerToggle)
				{
					if (inCross > 0 && inCross <= 64)
					{
						if (TriggerTeamcheck)
						{
							if (Trigger_EntityBase_TeamID != LocalPlayer_Team)
							{
								if (TriggerUseWPM)
								{
									if (TriggerBurstCheckbox)
									{
										TriggerBurstWPM();
									}
									else
									{
										TriggerShotWPM();
									}
								}
								else
								{
									if (TriggerBurstCheckbox)
									{
										TriggerBurstME();
									}
									else
									{
										TriggerShotME();
									}									
								}
							}
						}
						else
						{
							if (TriggerUseWPM)
							{
								if (TriggerBurstCheckbox)
								{
									TriggerBurstWPM();
								}
								else
								{
									TriggerShotWPM();
								}
							}
							else
							{
								if (TriggerBurstCheckbox)
								{
									TriggerBurstME();
								}
								else
								{
									TriggerShotME();
								}
							}
						}
					}
				}
			}
			else
			{
				if (GetAsyncKeyState(TriggerKey))
				{
					if (inCross > 0 && inCross <= 64)
					{
						if (TriggerTeamcheck)
						{
							if (Trigger_EntityBase_TeamID != LocalPlayer_Team)
							{
								if (TriggerUseWPM)
								{
									if (TriggerBurstCheckbox)
									{
										TriggerBurstWPM();
									}
									else
									{
										TriggerShotWPM();
									}
								}
								else
								{
									if (TriggerBurstCheckbox)
									{
										TriggerBurstME();
									}
									else
									{
										TriggerShotME();
									}
								}
							}
						}
						else
						{
							if (TriggerUseWPM)
							{
								TriggerShotWPM();
							}
							else
							{
								TriggerShotME();
							}
						}
					}
				}
			}
		}

		Sleep(1);
	}
}


void TriggerShotWPM()
{

	Sleep(TriggerDelay);
	Mem.Write<int>(Client + attack, 1);
	Sleep(10);
	Mem.Write<int>(Client + attack, 0);
}

void TriggerShotME()
{
	Sleep(TriggerDelay);

	mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
	Sleep(10);
	mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
}

void TriggerBurstWPM()
{
	Sleep(TriggerDelay);
	Mem.Write<int>(Client + attack, 1);
	Sleep(10);
	Mem.Write<int>(Client + attack, 0);
	Sleep(TriggerBurstDelay);
	Mem.Write<int>(Client + attack, 1);
	Sleep(10);
	Mem.Write<int>(Client + attack, 0);
	Sleep(TriggerBurstDelay);
	Mem.Write<int>(Client + attack, 1);
	Sleep(10);
	Mem.Write<int>(Client + attack, 0);
}

void TriggerBurstME()
{
	Sleep(TriggerDelay);
	mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
	Sleep(10);
	mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
	Sleep(TriggerBurstDelay);
	mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
	Sleep(10);
	mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
	Sleep(TriggerBurstDelay);
	mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
	Sleep(10);
	mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
}